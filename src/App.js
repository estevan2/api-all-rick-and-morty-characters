import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react'
import Chacarcters from './components/Chacarcters';

function App() {

  const [characterList, setCharacterList] = useState([])
  const [info, setInfo] = useState([])
  const [next, setNext] = useState(1)

  const addPage = () => {
    if (info.next !== null) {
      setNext(next + 1)
    }
  }

  const subPage = () => {
    if (next > 1) {
      setNext(next - 1)
    }
  }

  useEffect(() => {
    fetch(`https://rickandmortyapi.com/api/character?page=${next}`)
      .then((x) => x.json())
      .then((x) => {
        setCharacterList(x.results)
        setInfo(x.info)
      })
  }, [next])

  return (
    <div className="App">
      <main className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Chacarcters charList={characterList} add={addPage} sub={subPage}></Chacarcters>
      </main>
    </div>
  );
}

export default App;
