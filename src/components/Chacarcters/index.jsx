import CharCard from "../CharCard"
import "./style.css"

export default function Chacarcters({ charList, add, sub }) {

    return (
        <div>
            <h1>Meus Personagens</h1>
            <nav>
                <button className="prev" onClick={sub}>Anterior</button>
                <button className="next" onClick={add}>Próximo</button>
            </nav>
            <div className="charList">
                {charList.map((item, index) => (
                    <CharCard name={item.name} status={item.status} image={item.image} id={item.id} key={index}></CharCard>
                ))}
            </div>
            <nav>
                <button className="prev" onClick={sub}>Anterior</button>
                <button className="next" onClick={add}>Próximo</button>
            </nav>
        </div>
    )
}